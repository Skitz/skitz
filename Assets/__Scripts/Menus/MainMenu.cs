using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	
	public bool controls;
	public bool credits;
	
	public GUISkin mygui;
	public Texture2D StartGame;
	
	public float xHeight;
	public float yHeight;
	public float xOffset;
	public float yOffset;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI(){
		GUI.skin = mygui;
	if(controls)
	GUI.Window(1,new Rect(0,0,1024,768),Controls,"","Controls Page");
		else if(credits){
	GUI.Window(1,new Rect(0,0,1024,768),Credits,"","Credits Page");	
		}
		else{
	GUI.Window(1,new Rect(0,0,1024,768),MainMenuWindow,"","Background Image");	
		}
	}
	
	public void MainMenuWindow(int id){
		
	
		if(GUI.Button(new Rect(352,202,335.19f,100),"","MenuButtons"))
			Application.LoadLevel("MainGame");
		
		if(GUI.Button(new Rect(352,321,335.19f,100),"","MenuButtons"))
			credits = true;
			
		if(GUI.Button(new Rect(352,439,300,100),"", "MenuButtons"))
			controls = true;	
		
		if(GUI.Button(new Rect(352,559,300,100),"", "MenuButtons"))
			Application.Quit();	
	}
	
	public void Controls(int id){
				
		if(GUI.Button(new Rect(195,573,165,58),"","MenuButtons"))
			controls = false;
	}
	
		public void Credits(int id){
		
		if(GUI.Button(new Rect(366,521,174,79),"","MenuButtons"))
			credits = false;
	}
}
