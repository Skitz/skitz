using UnityEngine;
using System.Collections;

public enum PlatformType
{
	SmallPeice = 1,
	Prebuilt = 2,
}
