using UnityEngine;
using System.Collections;
using System;

public class TerrainPeice
{
	public TerrainPeice(String name, GameObject upperPlatform, GameObject lowerPlatform, PlatformType platformType, int blockWidth, int difficulty)
		: this(name, upperPlatform, lowerPlatform, platformType, 0, 0, blockWidth, difficulty)
	{
	}
	public TerrainPeice(String name, GameObject upperPlatform, GameObject lowerPlatform, PlatformType platformType, int offsetTop, int offsetBottom, int blockWidth, int difficulty)
	{
		Name = name;
		UpperPlatform = upperPlatform;
		LowerPlatform = lowerPlatform;
		Difficulty = difficulty;
		PlatformType = platformType;
		HasBeenInstantiated = false;
		BlockWidth = blockWidth;
		OffsetTop = offsetTop;
		OffsetBottom = offsetBottom;
	}
	
	public bool HasBeenInstantiated;
	public String Name;
	public GameObject LowerPlatform;
	public GameObject UpperPlatform;
	public GameObject LowerPlatformInstance;
	public GameObject UpperPlatformInstance;
	public PlatformType PlatformType;
	public int Difficulty;
	public int BlockWidth;
	
	public int OffsetTop;
	public int OffsetBottom;
	
	public float Width
	{
		get
		{
			return 5 * BlockWidth; //UpperPlatform.transform.localScale.x * BlockWidth;
		}
	}
}
