using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class LevelManager : MonoBehaviour {
	
	private List<TerrainPeice> _allPeices;
	
	private Queue<TerrainPeice> _activePeices;
	
	private bool _firstPeice = true;
	private PlatformType _levelPhase = PlatformType.SmallPeice;
	private System.Random _random = new System.Random();
	private float _nextPeicePlace;
	
	private int _pastPeices = 1;
	
	private GameObject _upPlayer;
	
	void Awake()
	{	
		GameObject good1 = Resources.Load("Good01") as GameObject;
		GameObject good2 = Resources.Load("Good02") as GameObject;
		GameObject good3 = Resources.Load("Good03") as GameObject;
		GameObject good4 = Resources.Load("Good04") as GameObject;
		GameObject good5 = Resources.Load("Good05") as GameObject;
		GameObject good6 = Resources.Load("Good06") as GameObject;
		GameObject good1b = Resources.Load("Good01") as GameObject;
		GameObject good2b = Resources.Load("Good02") as GameObject;
		GameObject good3b = Resources.Load("Good03") as GameObject;
		GameObject good4b = Resources.Load("Good04") as GameObject;
		GameObject good5b = Resources.Load("Good05") as GameObject;
		GameObject good6b = Resources.Load("Good06") as GameObject;	
		GameObject good1c = Resources.Load("Good01") as GameObject;
		GameObject good2c = Resources.Load("Good02") as GameObject;
		GameObject good3c = Resources.Load("Good03") as GameObject;
		GameObject good4c = Resources.Load("Good03") as GameObject;
		GameObject good5c = Resources.Load("Good05") as GameObject;
		GameObject good6c = Resources.Load("Good06") as GameObject;			
		
		GameObject good7 = Resources.Load("Good07") as GameObject;
		GameObject good8 = Resources.Load("Good08") as GameObject;
		GameObject good9 = Resources.Load("Good09") as GameObject;
     	GameObject good10 = Resources.Load("Good10")as GameObject;
		GameObject good11 = Resources.Load("Good11")as GameObject;
		GameObject good12 = Resources.Load("Good12")as GameObject;
		GameObject good13 = Resources.Load("Good13")as GameObject;
		GameObject good14 = Resources.Load("Good14")as GameObject;
		GameObject good15 = Resources.Load("Good15")as GameObject;
		GameObject good16 = Resources.Load("Good16")as GameObject;
		
		GameObject good7b = Resources.Load("Good07") as GameObject;
		GameObject good8b = Resources.Load("Good08") as GameObject;
		GameObject good9b = Resources.Load("Good09") as GameObject;
     	GameObject good10b = Resources.Load("Good10")as GameObject;
		GameObject good11b = Resources.Load("Good11")as GameObject;
		GameObject good12b = Resources.Load("Good12")as GameObject;
		GameObject good13b = Resources.Load("Good13")as GameObject;
		GameObject good14b = Resources.Load("Good14")as GameObject;
		GameObject good15b = Resources.Load("Good15")as GameObject;
		GameObject good16b = Resources.Load("Good16")as GameObject;		
		
		GameObject goodB = Resources.Load("GoodB")as GameObject;		
		GameObject goodD = Resources.Load("GoodD")as GameObject;		
		GameObject goodG = Resources.Load("GoodG")as GameObject;		
		GameObject goodH = Resources.Load("GoodH")as GameObject;		
		GameObject goodI = Resources.Load("GoodI")as GameObject;		
		
		GameObject goodJ1 = Resources.Load("Good_J_1")as GameObject;		
		
		GameObject goodB2 = Resources.Load("GoodB")as GameObject;		
		GameObject goodD2 = Resources.Load("GoodD")as GameObject;		
		GameObject goodG2 = Resources.Load("GoodG")as GameObject;		
		GameObject goodH2 = Resources.Load("GoodH")as GameObject;		
		GameObject goodI2 = Resources.Load("GoodI")as GameObject;		
		
		GameObject goodJ12 = Resources.Load("Good_J_1")as GameObject;			
	
		                                   
		GameObject bad1 = Resources.Load("Bad01") as GameObject;
		GameObject bad2 = Resources.Load("Bad02") as GameObject;
		GameObject bad3 = Resources.Load("Bad03") as GameObject;
		GameObject bad4 = Resources.Load("Bad04") as GameObject;
		GameObject bad5 = Resources.Load("Bad05") as GameObject;
		GameObject bad6 = Resources.Load("Bad06") as GameObject;
		GameObject bad1b = Resources.Load("Bad01") as GameObject;
		GameObject bad2b = Resources.Load("Bad02") as GameObject;
		GameObject bad3b = Resources.Load("Bad03") as GameObject;
		GameObject bad4b = Resources.Load("Bad04") as GameObject;
		GameObject bad5b = Resources.Load("Bad05") as GameObject;
		GameObject bad6b = Resources.Load("Bad06") as GameObject;
		GameObject bad1c = Resources.Load("Bad01") as GameObject;
		GameObject bad2c = Resources.Load("Bad02") as GameObject;
		GameObject bad3c = Resources.Load("Bad03") as GameObject;
		GameObject bad4c = Resources.Load("Bad04") as GameObject;
		GameObject bad5c = Resources.Load("Bad05") as GameObject;
		GameObject bad6c = Resources.Load("Bad06") as GameObject;		
		
		GameObject bad7 = Resources.Load("Bad07") as GameObject;
		GameObject bad8 = Resources.Load("Bad08") as GameObject;
		GameObject bad9 = Resources.Load("Bad09") as GameObject;
		GameObject bad10 = Resources.Load("Bad10") as GameObject;
		GameObject bad11 = Resources.Load("Bad11") as GameObject;
		GameObject bad12 = Resources.Load("Bad12") as GameObject;
		GameObject bad13 = Resources.Load("Bad13") as GameObject;
		GameObject bad14 = Resources.Load("Bad14") as GameObject;
		GameObject bad15 = Resources.Load("Bad15") as GameObject;
		GameObject bad16 = Resources.Load("Bad16") as GameObject;	
		
		GameObject bad7b = Resources.Load("Bad07") as GameObject;
		GameObject bad8b = Resources.Load("Bad08") as GameObject;
		GameObject bad9b = Resources.Load("Bad09") as GameObject;
		GameObject bad10b = Resources.Load("Bad10") as GameObject;
		GameObject bad11b = Resources.Load("Bad11") as GameObject;
		GameObject bad12b = Resources.Load("Bad12") as GameObject;
		GameObject bad13b = Resources.Load("Bad13") as GameObject;
		GameObject bad14b = Resources.Load("Bad14") as GameObject;
		GameObject bad15b = Resources.Load("Bad15") as GameObject;
		GameObject bad16b = Resources.Load("Bad16") as GameObject;	
		
		GameObject badB = Resources.Load("BadB")as GameObject;		
		GameObject badD = Resources.Load("BadD")as GameObject;		
		GameObject badG = Resources.Load("BadG")as GameObject;		
		GameObject badH = Resources.Load("BadH")as GameObject;		
		GameObject badI = Resources.Load("BadI")as GameObject;		
		
		GameObject badJ1 = Resources.Load("Bad_J_1")as GameObject;		
		
		GameObject badB2 = Resources.Load("BadB")as GameObject;		
		GameObject badD2 = Resources.Load("BadD")as GameObject;		
		GameObject badG2 = Resources.Load("BadG")as GameObject;		
		GameObject badH2 = Resources.Load("BadH")as GameObject;		
		GameObject badI2 = Resources.Load("BadI")as GameObject;		
		
		GameObject badJ12 = Resources.Load("Bad_J_1")as GameObject;			
		
		_allPeices = new List<TerrainPeice>();
						
		_allPeices.Add(new TerrainPeice("1", good1, bad1, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("2", good2, bad2, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("3", good3, bad3, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("4", good4, bad4, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("5", good5, bad5, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("6", good6, bad6, PlatformType.SmallPeice, 1, 1));	

		_allPeices.Add(new TerrainPeice("1b", good1b, bad1b, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("2b", good2b, bad2b, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("3b", good3b, bad3b, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("4b", good4b, bad4b, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("5b", good5b, bad5b, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("6b", good6b, bad6b, PlatformType.SmallPeice, 1, 1));
		
		_allPeices.Add(new TerrainPeice("1c", good1c, bad1c, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("2c", good2c, bad2c, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("3c", good3c, bad3c, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("4c", good4c, bad4c, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("5c", good5c, bad5c, PlatformType.SmallPeice, 1, 1));	
		_allPeices.Add(new TerrainPeice("6c", good6c, bad6c, PlatformType.SmallPeice, 1, 1));			
		
		_allPeices.Add(new TerrainPeice("7", good7, bad7, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("8", good8, bad8, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("9", good9, bad9, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("10", good10, bad10, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("11", good11, bad11, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("12", good12, bad12, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("13", good13, bad13, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("14", good14, bad14, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("15", good15, bad15, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("16", good16, bad16, PlatformType.SmallPeice, 1, 2));	
		
		_allPeices.Add(new TerrainPeice("7b", good7b, bad7b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("8b", good8b, bad8b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("9b", good9b, bad9b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("10b", good10b, bad10b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("11b", good11b, bad11b, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("12b", good12b, bad12b, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("13b", good13b, bad13b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("14b", good14b, bad14b, PlatformType.SmallPeice, 1, 3));	
		_allPeices.Add(new TerrainPeice("15b", good15b, bad15b, PlatformType.SmallPeice, 1, 2));	
		_allPeices.Add(new TerrainPeice("16b", good16b, bad16b, PlatformType.SmallPeice, 1, 2));	 		
		
		 _allPeices.Add(new TerrainPeice("18", goodB, badB, PlatformType.SmallPeice, 1, 2));;
		 _allPeices.Add(new TerrainPeice("20", goodD, badD, PlatformType.SmallPeice, 1, 2));
		 _allPeices.Add(new TerrainPeice("21", goodG, badG, PlatformType.SmallPeice, 3, 3));		
		 _allPeices.Add(new TerrainPeice("21", goodH, badH, PlatformType.SmallPeice, 3, 3));		
		 _allPeices.Add(new TerrainPeice("21", goodI, badI, PlatformType.SmallPeice, 2, 2));		
		
		_allPeices.Add(new TerrainPeice("J1", goodJ1, badJ1, PlatformType.SmallPeice, 2, 2));	
		
		 _allPeices.Add(new TerrainPeice("18", goodB2, badB2, PlatformType.SmallPeice, 1, 2));;
		 _allPeices.Add(new TerrainPeice("20", goodD2, badD2, PlatformType.SmallPeice, 1, 2));
		 _allPeices.Add(new TerrainPeice("21", goodG2, badG2, PlatformType.SmallPeice, 3, 3));		
		 _allPeices.Add(new TerrainPeice("21", goodH2, badH2, PlatformType.SmallPeice, 3, 3));		
		 _allPeices.Add(new TerrainPeice("21", goodI2, badI2, PlatformType.SmallPeice, 2, 2));		
		
		_allPeices.Add(new TerrainPeice("J1", goodJ12, badJ12, PlatformType.SmallPeice, 2, 2));		
		
		 _nextPeicePlace = -70f;
	
		 _activePeices = new Queue<TerrainPeice>();
		
		 _upPlayer = GameObject.FindGameObjectWithTag("UpPlayer");	
	}
	
	// Use this for initialization
	void Start () {
	
	}

	private TerrainPeice GetNextPeice()
	{
		//return _allPeices.First();
		
//		if(_firstPeice)
//		{
//			_firstPeice = false;
//			var prebuilt =  _allPeices.Where(p => p.PlatformType == PlatformType.Prebuilt).OrderBy(p => p.Difficulty).FirstOrDefault();
//			if(prebuilt != null)
//			{
//				return prebuilt;
//			}
//			else
//			{
//				return _allPeices.Shuffle().First();
//			}
//		}
		//else
		//{
			switch (_levelPhase) {
				
				case PlatformType.SmallPeice:
					
					// Switch to the next prebuilt level with a probability.
					if(_random.Next(1, 100) < 10)
					{
						_levelPhase	= PlatformType.Prebuilt;
					}
				
					int requiredDifficultyPeice = 0;
					var randomCategorySelector = _random.Next(0,100);
				
					float reciprocal = 1 - 1f / Math.Max(1,(_pastPeices/20f));
					float category1Limit = 90 - (60f * reciprocal); // Max 90, Min 30. R 60
					float category2Limit = 99 - (49f * reciprocal); // Max 99, Min 45. R 49
					float category3Limit = 100 - (40f * reciprocal);// Max 100, Min 60. R = 40
				
					//Debug.Log(String.Format("P({0}) R({1}) {2} {3} {4}", _pastPeices, recepricol, category1Limit, category2Limit, category3Limit));
					if(randomCategorySelector <= category1Limit)
					{
						requiredDifficultyPeice = 1;
					}
					else if(requiredDifficultyPeice <= category2Limit )
					{
						requiredDifficultyPeice = 2;
					}
					else if(requiredDifficultyPeice <= category3Limit)
					{
						requiredDifficultyPeice = 3;
					}				
				
					// Select a random small peice.
					var smallPeice = _allPeices.Where(p => p.PlatformType == PlatformType.SmallPeice 
				                                  		&& p.Difficulty == requiredDifficultyPeice)
													.Shuffle().FirstOrDefault();
					if(smallPeice != null)
					{
						return smallPeice;
					}
					else
					{
						return _allPeices.Shuffle().First();
					}
				
					
				case PlatformType.Prebuilt:
					// Select a random prebuilt peice and immediatly return back to small peices.
					_levelPhase = PlatformType.SmallPeice;
					var prebuilt =  _allPeices.Where(p => p.PlatformType == PlatformType.Prebuilt).Shuffle().FirstOrDefault();					
					if(prebuilt != null)
					{
						return prebuilt;
					}
					else
					{
						return _allPeices.Shuffle().First();
					}				
					 
				default:
					//Debug.Log("Unknown peice.");
					return _allPeices.First();
			}
		//}
	}
	
	void Update () 
	{	
		bool changed = false;
				
		if(_activePeices.Count() > 0)
		{
			// First dequeue all the peices that have been passed.
			var nextPeiceToRemove = _activePeices.Peek();
			
			while(nextPeiceToRemove != null && nextPeiceToRemove.UpperPlatformInstance.transform.position.x < _upPlayer.transform.position.x -30)
			{
				changed = true;
				
				//Debug.Log("make inactive");
				var peiceToDequeue = _activePeices.Dequeue();
				_pastPeices++;
				peiceToDequeue.UpperPlatformInstance.active = false;
				peiceToDequeue.LowerPlatformInstance.active = false;
				_allPeices.Add(peiceToDequeue);
				
				// Peek at the next peice.
				if(_activePeices.Count() > 0)
				{
					nextPeiceToRemove = _activePeices.Peek();
				}
				else
				{
					nextPeiceToRemove = null;
				}
			}
		}
		
		while(_activePeices.Sum(p=>p.Width) < 100 && _allPeices.Count > 0)
		{
			changed = true; 
			
			TerrainPeice nextPeice = GetNextPeice();
			//Debug.Log(String.Format("Length = {0}, Peice {1} Count {2}, Position {3}" ,_activePeices.Sum(p=>p.Width), nextPeice.Name, _activePeices.Count(), _nextPeicePlace));
			_activePeices.Enqueue(nextPeice);
			_allPeices.Remove(nextPeice);
			
			_nextPeicePlace += nextPeice.Width / 2;
			if(!nextPeice.HasBeenInstantiated)
			{
				nextPeice.HasBeenInstantiated = true; 
				nextPeice.UpperPlatformInstance = Instantiate(nextPeice.UpperPlatform, new Vector3(_nextPeicePlace + nextPeice.OffsetTop,10f,-0f), Quaternion.identity) as GameObject;
				nextPeice.LowerPlatformInstance = Instantiate(nextPeice.LowerPlatform, new Vector3(_nextPeicePlace + nextPeice.OffsetBottom,-28f,-0f), Quaternion.identity) as GameObject;
				
				//Debug.Log(String.Format("Name {0} Position {1}, Requested {2}, Actual {3}" ,nextPeice.Name, nextPeice.UpperPlatform.transform.position.x, _nextPeicePlace, nextPeice.UpperPlatformInstance.transform.position.x));
			}
			else
			{
				nextPeice.UpperPlatformInstance.transform.position = new Vector3(_nextPeicePlace,10f,-0f);
				nextPeice.LowerPlatformInstance.transform.position = new Vector3(_nextPeicePlace,-28f,-0f);
				nextPeice.UpperPlatformInstance.active = true;
				nextPeice.LowerPlatformInstance.active = true;
				
			}
			_nextPeicePlace += nextPeice.Width / 2;
		}
		
		if(changed)
		{
			String active = " ";
			foreach (var peice in _activePeices)
			{
				active += peice.Name + " ";
			}
			
			Debug.Log(active);
		}
	}	
}
