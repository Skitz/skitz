using System;
using UnityEngine;
using System.Collections;

public class HealthPickUp : MonoBehaviour {

	public static HealthPickUp Instance;
	
	public GameObject playerTransform;
	public GameObject playerTransform2;
	public Transform myTransform;
		
	public GameObject starSplat2;
	public GameObject starSplat;
	public bool nonCollectable;
	
	public DateTime time;
	
	// Use this for initialization
	void Start () {
		myTransform = transform;
		playerTransform = GameObject.FindGameObjectWithTag("UpPlayer");
		playerTransform2 = GameObject.FindGameObjectWithTag("DownPlayer");
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(time < DateTime.Now && !transform.GetComponent<MeshRenderer>().enabled)
		{
			transform.GetComponent<MeshRenderer>().enabled = true;
		}
	}
	
	void OnTriggerEnter(Collider other){
		
		if(other.name == "downPlayer")
		{
			GameObject pe = Instantiate(starSplat,playerTransform2.transform.position,Quaternion.identity)as GameObject;
		}
		if(other.name == "upPlayer")
		{
			GameObject pe2 = Instantiate(starSplat2,playerTransform.transform.position, Quaternion.identity) as GameObject;
		}
		
		GameMaster.Instance.Health += 5;
		
		//System.Runtime.Remoting.Contexts.Context mainThreadContext = System.Threading.Thread.CurrentContext;
		
		if(!nonCollectable)
		{
			transform.GetComponent<MeshRenderer>().enabled = false;
			time = DateTime.Now.AddSeconds(1);
			
		}
	}
}
