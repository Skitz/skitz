using UnityEngine;
using System.Collections;

public class PoisionPickUp : MonoBehaviour {

	
	public GameObject playerTransform;
	public GameObject playerTransform2;
	public Transform myTransform;
		
	public GameObject bloodSplat;
	public GameObject bloodSplat2;
	public bool nonCollectable;
	// Use this for initialization
	void Start () {
		myTransform = transform;
		playerTransform = GameObject.FindGameObjectWithTag("UpPlayer");
		playerTransform2 = GameObject.FindGameObjectWithTag("DownPlayer");
		
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter(Collider other){
		if(other.name == "downPlayer"){
		GameObject pe = Instantiate(bloodSplat2,playerTransform2.transform.position,Quaternion.identity)as GameObject;
			}
		if(other.name == "upPlayer"){
		GameObject pe2 = Instantiate(bloodSplat,playerTransform.transform.position, Quaternion.identity) as GameObject;
			}
	if(nonCollectable){
		GameMaster.Instance.Health -= 5;
			
	}
		else{
		Destroy(gameObject);
		GameMaster.Instance.Health -= 5;	
		}
	}
}
