using UnityEngine;
using System.Collections;

public class DownPlayerController : PlayerBaseController {

	// Use this for initialization
	void Start () {
		this.Player = PlayerEnum.DownPlayer;
	}
	
	// Update is called once per frame
	public override void Update () {
		
		if(Input.GetKey(KeyCode.W))
		{
			animation.Play("Jump");	
			
			Jump();
		}		
		if(!animation.IsPlaying("Jump"))
			animation.CrossFade("Run");
		if(Input.GetKey(KeyCode.S))
		{
			animation.Play("GoingDown");
			if(Input.GetKeyUp(KeyCode.DownArrow)){
			animation.Blend("GetUp",1f,0.0001f);
			animation["GetUp"].blendMode = AnimationBlendMode.Blend;
			}
			Duck();
		}
		
		base.Update();
	}
}
