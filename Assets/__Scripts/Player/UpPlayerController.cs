using UnityEngine;
using System.Collections;

public class UpPlayerController : PlayerBaseController {

	// Use this for initialization
	void Start () {
		this.Player = PlayerEnum.UpPlayer;
	}
	
	// Update is called once per frame
	public override void Update () {
		
		if(Input.GetKey(KeyCode.UpArrow))
		{
			animation.Play("Jump");				
			Jump();
		}
		
		if(!animation.IsPlaying("Jump"))
		animation.CrossFade("Run");
		
		if(Input.GetKey(KeyCode.DownArrow))
		{
			animation.Play("GoingDown");
			if(Input.GetKeyUp(KeyCode.DownArrow)){
			animation.Blend("GetUp",1f,0.0001f);
			animation["GetUp"].blendMode = AnimationBlendMode.Blend;
			}
			Duck();
		}
		
		base.Update();
	}
}
