using UnityEngine;
using System.Collections;
	
public class PlayerBaseController : MonoBehaviour {
	
	public float speed = 0.1f;
    public float jumpForce = 300.0f;
	
	public float minSpeed = 5;
	public float speedIncrease = 5;
	public float curSpeed;
	
	protected PlayerEnum Player {get;set;}
	void Awake(){
		speed = 0.1f;
	}
	// Use this for initialization
	void Start () {
		
		switch (Player) {
			case PlayerEnum.UpPlayer:
				break;
			
			case PlayerEnum.DownPlayer:
				break;
			
			default:
				//Debug.Log("Unknown player");
				break;
		}
	}
	
	// Update is called once per frame
	public virtual void Update () {
		speed = speed * 1.00025f;
		if(PauseMenu.Instance.pause == true){
		transform.Translate(new Vector3(0,0,0));	
		}
		else{
		transform.Translate(new Vector3(0f,0f,speed));
		}
		//Debug.Log("curSpeed: " + speed + 0.01f * Time.timeSinceLevelLoad);
	}
	
	protected void Jump()
	{	
		bool isGrounded = false;
		switch (Player) 
		{
			case PlayerEnum.UpPlayer:
				isGrounded = transform.position.y < 15.64029;
				
				break;
			
			case PlayerEnum.DownPlayer:
				isGrounded = transform.position.y < -22.35971;
				
				break;
		}
		if(isGrounded)
		{
			Debug.Log("Actual Jump");
			rigidbody.AddRelativeForce(transform.up * jumpForce, ForceMode.Impulse);
		}
	}
	
	protected void Duck()
	{
		animation.Play("GoingDown");
			if(Input.GetKeyUp(KeyCode.DownArrow)){
			animation.Blend("GetUp",1f,0.0001f);
			animation["GetUp"].blendMode = AnimationBlendMode.Blend;
		}
		if(!animation.IsPlaying("GetUp")){
			animation.CrossFade("Run");
		}
	}
}