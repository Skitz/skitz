using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {
	
	public GameObject UpPlayer;
	public GameObject DownPlayer;
	
	public GameObject CameraUpPlayer;
	public GameObject CameraDownPlayer;
	
		
	void Awake()
	{
		GameObject upPlayer = Instantiate(UpPlayer, new Vector3(-40,20,0), Quaternion.identity) as GameObject;
		GameObject downPlayer = Instantiate(DownPlayer, new Vector3(-40,-20,0), Quaternion.identity) as GameObject;
		
		upPlayer.transform.Rotate(new Vector3(0,90,0));
		downPlayer.transform.Rotate(new Vector3(0,90,0));
		
		upPlayer.name = "upPlayer";
		downPlayer.name = "downPlayer";
		
		GameObject upCamera = Instantiate(CameraUpPlayer, new Vector3(-40,20,-10), Quaternion.identity) as GameObject;
		GameObject downCamera = Instantiate(CameraDownPlayer, new Vector3(-40,-20,-10), Quaternion.identity) as GameObject;		
		
		upCamera.GetComponent<SmoothFollow>().target = upPlayer.transform;		
		downCamera.GetComponent<SmoothFollow>().target = downPlayer.transform;
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
