using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class LightManager : MonoBehaviour {
	
	public GameObject UpLightPrefab;
	public GameObject DownLightPrefab;
	
	private Queue<GameObject> _activeUpLights;
	private Queue<GameObject> _activeDownLights;
	
	private System.Random _random;
	
	private GameObject _upPlayer;
	
	private int lastLightPosition;
	
	void Awake() 
	{
		_random = new System.Random();
	
		_activeUpLights = new Queue<GameObject>();
		_activeDownLights = new Queue<GameObject>();
		
		for (int lightCounter = 0; lightCounter < 3; lightCounter++)
		{
			lastLightPosition = (-40 + lightCounter*10);
			
			GameObject upLightInstance = Instantiate(UpLightPrefab, new Vector3(lastLightPosition, 26, -7), Quaternion.Euler(new Vector3(_random.Next(0,90),_random.Next(0,90),_random.Next(0,90)))) as GameObject;	
			GameObject downLightInstance = Instantiate(DownLightPrefab, new Vector3(lastLightPosition, -14, -7), Quaternion.Euler(new Vector3(_random.Next(0,90),_random.Next(0,90),_random.Next(0,90)))) as GameObject;	
			
			_activeUpLights.Enqueue(upLightInstance);
			_activeDownLights.Enqueue(downLightInstance);
		}				
	}
	// Use this for initialization
	void Start () {
		_upPlayer = GameObject.FindGameObjectWithTag("UpPlayer");	
	}
	
	// Update is called once per frame
	void Update () {
		
		if(_activeUpLights.Count > 0){
			
			GameObject nextLightToRemove = _activeUpLights.Peek();
			
			if(nextLightToRemove.transform.position.x <  _upPlayer.transform.position.x)
			{
				_activeUpLights.Dequeue();	
				_activeDownLights.Dequeue();	
				
				lastLightPosition += 10;
				
				GameObject upLightInstance = Instantiate(UpLightPrefab, new Vector3(lastLightPosition, 26, -7), Quaternion.Euler(new Vector3(_random.Next(0,90),_random.Next(0,90),_random.Next(0,90)))) as GameObject;	
				GameObject downLightInstance = Instantiate(DownLightPrefab, new Vector3(lastLightPosition, -14, -7), Quaternion.Euler(new Vector3(_random.Next(0,90),_random.Next(0,90),_random.Next(0,90)))) as GameObject;	
				_activeUpLights.Enqueue(upLightInstance);				
				_activeDownLights.Enqueue(downLightInstance);				
			}
		}		
	}
}
