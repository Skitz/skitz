using UnityEngine;
using System.Collections;

public class BearTrap : MonoBehaviour {

	
	public GameObject playerTransform;
	public GameObject playerTransform2;
	public Transform myTransform;
	
	public GameObject bloodSplat;
	public GameObject bloodSplat2;
		
	public bool nonCollectable;
	// Use this for initialization
	void Start () {
		myTransform = transform;
		playerTransform = GameObject.FindGameObjectWithTag("upPlayer");
		playerTransform2 = GameObject.FindGameObjectWithTag("downPlayer");
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter(Collider other){
		if(other.name == "upPlayer")
		{
			GameObject bs = Instantiate(bloodSplat,playerTransform.transform.position,Quaternion.identity) as GameObject;
			animation.Play();
			if(nonCollectable){
			GameMaster.Instance.Health -= 5;	
		}
	}
		
	if(other.name == "downPlayer")
	{
		GameObject bs = Instantiate(bloodSplat,playerTransform2.transform.position,Quaternion.identity) as GameObject;
		animation.Play();
		if(nonCollectable)
		{
			GameMaster.Instance.Health -= 5;	
		}
	   }
	}
	
	void OnTriggerExit()
	{
		animation.Rewind();
	}
}