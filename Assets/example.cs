using UnityEngine;
using System.Collections;

public class example : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(Input.GetKey(KeyCode.DownArrow)){
			animation.Play("GoingDown");
			if(Input.GetKeyUp(KeyCode.DownArrow)){
			animation.Blend("GetUp",1f,0.0001f);
			animation["GetUp"].blendMode = AnimationBlendMode.Blend;
			}
		}
		if(!animation.IsPlaying("GetUp"))
			animation.CrossFade("Run");
		
		if(Input.GetKey(KeyCode.UpArrow)){
			animation.Play("Jump");	
		}
		if(!animation.IsPlaying("Jump"))
			animation.CrossFade("Run");
		
		if(Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.UpArrow)){
			animation.Play("Run");	
		}
	}
}
