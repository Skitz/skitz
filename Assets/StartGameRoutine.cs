using UnityEngine;
using System.Collections;

public class StartGameRoutine : MonoBehaviour {
	
	IEnumerator WaitAndPrint() {
		yield return new WaitForSeconds(5);
		print("WaitAndPrint " + Time.time);
		}
	IEnumerator Start() {
		print("Starting " + Time.time);
		yield return WaitAndPrint();
		print("Done " + Time.time);
		}
}